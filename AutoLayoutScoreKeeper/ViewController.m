//
//  ViewController.m
//  AutoLayoutScoreKeeper
//
//  Created by Jordan Hipwell on 2/23/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *playerOneTextField;
@property (weak, nonatomic) IBOutlet UITextField *playerTwoTextField;
@property (weak, nonatomic) IBOutlet UILabel *playerOneScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *playerTwoScoreLabel;
@property (weak, nonatomic) IBOutlet UIStepper *playerOneStepper;
@property (weak, nonatomic) IBOutlet UIStepper *playerTwoStepper;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    //dismiss keyboard when user taps Done
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Methods

- (IBAction)playerStepperTouched:(UIStepper *)sender {
    NSString *newScore = [NSString stringWithFormat:@"%.f", sender.value]; //truncate at decimal
    
    if ([sender isEqual:self.playerOneStepper]) {
        self.playerOneScoreLabel.text = newScore;
    } else if ([sender isEqual:self.playerTwoStepper]) {
        self.playerTwoScoreLabel.text = newScore;
    }
}

- (IBAction)resetButtonTouched:(UIBarButtonItem *)sender {
    self.playerOneStepper.value = 0;
    self.playerOneScoreLabel.text = [self initialScoreString];
    
    self.playerTwoStepper.value = 0;
    self.playerTwoScoreLabel.text = [self initialScoreString];
}

#pragma mark - Navigation

- (IBAction)unwindFromHistory:(UIStoryboardSegue *)segue {
    //form sheet doesn't dismiss on iPad, manually dismiss
    if (![segue.sourceViewController isBeingDismissed]) {
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

#pragma mark - Helper Methods

- (NSString *)initialScoreString {
    return [NSString stringWithFormat:@"%d", 0];
}

@end
