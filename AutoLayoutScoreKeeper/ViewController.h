//
//  ViewController.h
//  AutoLayoutScoreKeeper
//
//  Created by Jordan Hipwell on 2/23/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@end
