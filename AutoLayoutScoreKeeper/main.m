//
//  main.m
//  AutoLayoutScoreKeeper
//
//  Created by Jordan Hipwell on 2/23/15.
//  Copyright (c) 2015 Jordan Hipwell. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
